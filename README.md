## Build the container

`docker-compose up -d --no-deps --build web`

## Run the container

`docker-compose run web`

## Access the app

Over at http://localhost:8080/