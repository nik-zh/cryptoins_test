 var HtmlWebpackPlugin = require('html-webpack-plugin');
 var CopyWebpackPlugin = require('copy-webpack-plugin');
 var path = require('path');
 var webpack = require('webpack');

 module.exports = {
	entry: './js/main.js',

	output: {
		path: path.resolve(__dirname, 'build'),
		filename: 'main.bundle.js'
	},

	module: {
		rules: [{
			test: /\.(scss)$/,
			use: [{
				loader: 'style-loader',
			}, {
				loader: 'css-loader',
			}, {
				loader: 'postcss-loader',
				options: {
					plugins: function () {
						return [
							require('precss'),
							require('autoprefixer')
						];
					}
				}
			}, {
				loader: 'sass-loader'
			}]
		}]
	},

	plugins: [
		new webpack.ProvidePlugin({
			$: 'jquery',
			jQuery: 'jquery'
		}),
		new HtmlWebpackPlugin({ template: './index.html' }),
		new CopyWebpackPlugin([{ from: 'assets', to: 'assets' }])
    ],

	stats: {
		colors: true
	},
 };