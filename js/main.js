require("../css/_main.scss");
require("bootstrap");

String.prototype.capitalize = function() {
	if (this.length <= 0) return "";
    return this.charAt(0).toUpperCase() + this.slice(1);
}

var contents = [ // can be loaded dynamically
	{value: "bibox", name: "bibox", image: "/assets/bibox.png"},
	{value: "binance", name: "binance", image: "/assets/binance.png"},
	{value: "bit-z", name: "bit-z", image: "/assets/bit-z.png"},
	{value: "bitfinex", name: "bitfinex", image: "/assets/bitfinex.png"},
	{value: "bitstamp", name: "bitstamp", image: "/assets/bitstamp.png"},
	{value: "bittrex", name: "bittrex", image: "/assets/bittrex.png"},
	{value: "blockchain", name: "blockchain", image: "/assets/blockchain.png"},
	{value: "cobinhood", name: "cobinhood", image: "/assets/cobinhood.png"},
	{value: "coinbase", name: "coinbase", image: "/assets/coinbase.png"},
	{value: "coinbene", name: "coinbene", image: "/assets/coinbene.png"},
	{value: "coincheck", name: "coincheck", image: "/assets/coincheck.png"},
	{value: "coinex", name: "coinex", image: "/assets/coinex.png"},
	{value: "digifinex", name: "digifinex", image: "/assets/digifinex.png"},
	{value: "hitbtc", name: "hitbtc", image: "/assets/hitbtc.png"},
	{value: "huobi", name: "huobi", image: "/assets/huobi.png"},
	{value: "kraken", name: "kraken", image: "/assets/kraken.png"},
	{value: "ledger", name: "ledger", image: "/assets/ledger.png"},
	{value: "localbitcoins", name: "localbitcoins", image: "/assets/localbitcoins.png"},
	{value: "lykke", name: "lykke", image: "/assets/lykke.png"},
	{value: "myetherwallet", name: "myetherwallet", image: "/assets/myetherwallet.png"},
	{value: "okex", name: "okex", image: "/assets/okex.png"},
	{value: "poloniex", name: "poloniex", image: "/assets/poloniex.png"},
	{value: "quoine", name: "quoine", image: "/assets/quoine.png"},
	{value: "shapeshift", name: "shapeshift", image: "/assets/shapeshift.png"},
	{value: "testnet", name: "testnet", image: "/assets/testnet.png"},
	{value: "topbtc", name: "topbtc", image: "/assets/topbtc.png"},
	{value: "trezor", name: "trezor", image: "/assets/trezor.png"},
	{value: "upbit", name: "upbit", image: "/assets/upbit.png"},
	{value: "wex", name: "wex", image: "/assets/wex.png"},
	{value: "yobit", name: "yobit", image: "/assets/yobit.png"},
	{value: "zaif", name: "zaif", image: "/assets/zaif.png"}
]

$(document).ready(function() {
	let $dropdown = $("#select-exchange-list");
	let $input = $("#select-exchange-input");
	let $input_image = $("#select-exchange-input-image");
	contents.sort((a,b) => (a.name > b.name) ? 1 : b.name > a.name ? -1 : 0);

	$.each(contents, function() {
		$dropdown.append($(`<li class='dropdown-item' value="${this.value}"><div class='dropdown-item-inner'>${this.name.capitalize()}<img class='item-image' src="${this.image}"></img></div></li>`));
	});

	function clearImage() {
		if ($(this).val().length <= 0) {
			$input_image.attr("src", "");
		}
	}

	function filterList() {
		let value = $(this).val().toLowerCase();
		console.log(value);
		$("#select-exchange-list li").filter(function() {
			$(this).toggle($(this).text().toLowerCase().startsWith(value))
		});
	}

	$input
	.on("keyup", function() {
		filterList.call(this);
		clearImage.call(this);
	})
	.blur(function() {
		filterList.call(this);
		clearImage.call(this);
	})
	.focus(function() {
		filterList.call(this);
	});

	$dropdown.find("li")
	.click(function() {
		var value = $(this).attr('value');
		var item = contents.find(item => item.value === value);
		if (item) {
			$input.val(item.name.capitalize());
			$input_image.attr("src", item.image);
			$input_image.css("height", $input.height());
			// send "value" to the Model from here
		}
	})
});