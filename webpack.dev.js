 var path = require('path');
 var merge = require('webpack-merge');
 var common = require('./webpack.common.js');

 module.exports = merge(common, {
	mode: 'development',

	devServer: {
		contentBase: './build',
		host: '0.0.0.0',
		port: 8080,
		open: true,
		public: 'localhost:8080'
	},

	devtool: 'source-map',
 });